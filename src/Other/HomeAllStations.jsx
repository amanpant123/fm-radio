import axios from 'axios'
import React,{useState,useEffect} from 'react'

const HomeAllStations = ({country}) => {
   const [Country,setCountry]=useState([])
  useEffect(()=>{
    const fetch=async()=>{
        const data=await axios.get('https://nl1.api.radio-browser.info/json/stations/search',{
            params:{
                country:country,
                limit:10
            }
        })
        setCountry(data.data)
    }
    fetch()
  },[country])


    return (
        <div style={{display:'flex'}}>
            {Country.map((item,index)=>
                <div style={{margin:'20px 20px 20px 0'}} key={index}>
                   <img src={item.favicon} alt="" style={{width:'80px'}}/>
                   <p>{item.name}</p>
                </div>
            )}
            <p>See All</p>
        </div>
    )
}

export default HomeAllStations
