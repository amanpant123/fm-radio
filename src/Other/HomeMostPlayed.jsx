import axios from 'axios'
import React,{useState,useEffect} from 'react'

const HomeMostPlayed = ({country}) => {
  const [MostPlayed,setMostPlayed]=useState([])
  useEffect(()=>{
      const fetch=async()=>{
          const data=await axios.get('https://nl1.api.radio-browser.info/json/stations/search',{
              params:{
                  country:country,
                  order:"clickcount",
                  limit:10
              }
          })
          setMostPlayed(data.data)
      }
      fetch()
  },[country])

    return (
        <div style={{display:'flex',alignItems:'center'}}>
            {MostPlayed.map((item,index)=>
            <div key={index} style={{margin:'20px 20px 20px 0'}}>
                <img src={item.favicon} alt="" style={{width:'70px'}}/>
                <p>{item.name}</p>
            </div>)}
            <p>See All</p>
        </div>
    )
}

export default HomeMostPlayed
