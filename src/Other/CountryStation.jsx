import React from 'react'
import styled from 'styled-components'
// import {useNavigate} from 'react-router-dom'
// import { useEffect } from 'react/cjs/react.development'



const CountryStation = ({text,data,handleLocal}) => {
// const [radio,setRadio]=useState([])
// const recentPlayed=[]

// const handleClick=(item)=>{
//     recentPlayed.push(item)
//   localStorage.setItem('user',JSON.stringify(recentPlayed))
// }

// const HandleClick=(item)=>{
//     localStorage.setItem('user',JSON.stringify(item))
//     const music = JSON.parse(localStorage.getItem('user'));
//     setRadio(music)
// }

//   const navigate=useNavigate()
//   if(radio){
//       navigate('/home',{state:radio})
//   }

// useEffect(()=>{
//     const addData=()=>{
//         data1.push(radio)
//         console.log(data1)
//     }
//     addData()
// },[radio])

    return (
        <Container>
            <Text>
                {text}
            </Text>
            <MapContainer>
            {data&&data.map((item,index)=>(
                <IconContainer key={index} onClick={
                ()=>  handleLocal(item)  }>
                <Icon src={item.favicon?item.favicon:'https://hindiradios.com/assets/img/radio-logos/hungama-90s-super-hits.jpg'}/>
                <IconText>
                      {item.name}
                </IconText>
                </IconContainer>
            ))}
            </MapContainer>
        </Container>
    )
}

export default CountryStation

const Container=styled.div`

`
const Text=styled.p`
font-size: 18px;
font-weight: bold;
`
const IconContainer=styled.div`
max-width: 100px;
margin: 20px 20px 20px 0;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
cursor: pointer;
`
const Icon=styled.img`
min-width: 70px;
max-width:80px;
border-radius: 20px;
object-fit: cover;
transition-duration: 0.3s;
&:hover{
    transform: scale(1.3);
}
`
const IconText=styled.div`
font-size: 13px;
width: 90%;
text-align: center;
margin-top: 20px;
`
const MapContainer=styled.div`
display: grid;
grid-template-columns: repeat(7,minmax(0,1fr));
`