import React,{useState} from 'react'
import styled from 'styled-components'
import CountrySearch from './CountrySearch'
import SearchBox from './SearchBox'
const SearchCountry = () => {
    const DATA=[
        {
            name:'India',
            station:1,
            icon:'https://flagicons.lipis.dev/flags/4x3/in.svg'
        },
        {
            name:'Italy',
            station:3,
            icon:'https://flagicons.lipis.dev/flags/4x3/it.svg'

        },
        {
            name:'Portugal',
            station:4,
            icon:'https://flagicons.lipis.dev/flags/4x3/pt.svg'

        },
        {
            name:'Spain',
            station:2,
            icon:'https://flagicons.lipis.dev/flags/4x3/es.svg'

        },
        {
            name:'Argentina',
            station:6,
            icon:'https://flagicons.lipis.dev/flags/4x3/ar.svg'

        }
    ]
    const [filter,setFilter]=useState('')
    const [filteredData,setFilteredData]=useState(DATA)
    
    // useEffect(()=>{
    //    filter===''&&setFilteredData(DATA)
    // },[DATA,filter])
   

    const handleChange=(e)=>{
        e.preventDefault()
        if(e.target.value.length>0){
            setFilter(e.target.value)
        setFilteredData(filteredData.filter((country=>country.name.toLowerCase().includes(filter.toLowerCase()))))
        }else {
            setFilteredData(DATA)
        }
       
      }
       
    return (
        <Container>
            <SearchBox placeholder="Search Country" handleChange={handleChange}/>
            <CountrySearch Data={filteredData}/>
        </Container>
    )
}

export default SearchCountry

const Container=styled.div`
top: 80px;
position: relative;
`