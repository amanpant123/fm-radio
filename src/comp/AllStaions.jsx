import React,{useState} from 'react'
import styled from 'styled-components'
import   'react-h5-audio-player/lib/styles.css';
import AudioPlayer, { RHAP_UI } from 'react-h5-audio-player';
// import ReactPlayer from 'react-player'
// import AudioPlayer from './Audio';
const AllStaions = ({Data}) => {
    const [audio,setAudio]=useState('')

    // const HandleAudio=(urlAudio)=>{
    //     return (
    //         <AudioContainer>
    //         {/* <ReactAudioPlayer
    //    src={urlAudio}
    //    controls={true}
    //    autoplay
    //    /> */}
    //    <h1 style={{color:'red'}}>Aman</h1>
    //    </AudioContainer>
    //     )
    // }
  console.log(audio)
    return (<>
       <Container>
           {audio===""? '':(
               <AudioContainer>
                       <AudioPlayer
                          autoPlay={false}
                          src={audio?.url}
                          onPlay={e => console.log("onPlay")}
                          showJumpControls={true} 
                        style={{
                            width:'85vw',
                            color:'black',
                            backgroundColor:'rgba(255,255,255,0.1)',
                        }}
                        layout="horizontal"
   
                        />
                        <GoogleContainer>
                        <GoogleIcon src="images/google.png"/>
                        <GoogleText>Install App now </GoogleText>
                        </GoogleContainer>
                        <TextContainer>
                            <TextName>{audio?.name}</TextName>
                            <TextLanguage>
                               language : {audio?.language}
                            </TextLanguage>
                        </TextContainer>
                        {/* <ReactPlayer1 
                        url={audio} 
                        controls={true} 
                        playing={false}
                        
                        /> */}
                    {/* <AudioPlayer rc={audio}/>                      */}
               </AudioContainer>
           

           )}
       <Text>
               All Stations
           </Text>
           <Container1>
           {
               Data.map((item,index)=>
             
                <Flex onClick={()=>setAudio({
                   name:item.name,
                   language:item.language,
                   url:item.url
                })}>
                    <Icon src="https://hindiradios.com/assets/img/radio-logos/hungama-90s-super-hits.jpg"/>
                    <Name>
                       {item.name}
                    </Name>
                </Flex>
          
               )
           }
           </Container1>
          
       </Container>
       </>
    )
}

export default AllStaions

const Container=styled.div`
color: black;
min-height: 100vh;
top: 100px;
width: 80%;
margin: auto;
`
const Text=styled.div`
color: white;
font-size: 30px;
margin-left: 80px;
`
const Flex=styled.div`
cursor: pointer;
max-height: 120px;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`
const Icon=styled.img`
max-width: 80px;
height: 100px;
object-fit: contain;
border-radius: 30px;
`
    
const Name=styled.p`
color: white;
font-size: 12px;
text-align: center;
`
const Container1=styled(Container)`
display: flex;
`
const AudioContainer=styled.div`
z-index: 100;
bottom: 0;
color: black;
position: fixed;
`
const GoogleContainer=styled.div`
position: relative;
max-width: 200px;
top: -57px;
left: 670px;
display: flex;
justify-content: center;
align-items: center;
`
const GoogleIcon=styled.img`
width: 140px;
object-fit: contain;
`
const GoogleText=styled.p`
font-size:12px;
`
const TextContainer=styled.div`
position: relative;
top: -100px;
display: flex;
width: 200px;
margin-left: 60px;

`
const TextName=styled.p`
color: white;

`
const TextLanguage=styled.p`
color: white;
`