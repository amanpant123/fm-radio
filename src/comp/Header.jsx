import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'
import {AiOutlineDownload,AiFillFacebook,AiOutlineInstagram,AiOutlineFlag} from 'react-icons/ai'
const Header = ({screen}) => {
    return (
       <Container screen>
           <Link to="/">
           <Icon src='images/fm1.png'/>           
           </Link>
           <HeaderContainer>
               <Text>Install app for better experience</Text>
               <AiOutlineDownload size="30px"/>
               <Text>Follow us on Social Media</Text>
               <AiFillFacebook size="30px" />
               <AiOutlineInstagram size="30px"/>
               <Icon2>
                 <AiOutlineFlag size="20px"/>
               </Icon2>
           </HeaderContainer>
       </Container>
    )
}

export default Header

const Container=styled.div`
min-height: ${props=>props.screen?'50px':'70px'};
width: 100%;
background-color: rgba(137, 20, 222, 0.87);
color: white;
display: flex;
justify-content: space-between;
align-items: center;
position: sticky;
`
const Icon=styled.img`
max-width: 200px;
margin-left: 5%;
`
const HeaderContainer=styled.div`
display: flex;
max-width: 70%;
`
const Text=styled.div`
max-width: 150px;
margin-left: 20px;
`


const Icon2=styled.div`
margin-left: 40px;
margin-right: 20px;
border: 2px solid white;
border-radius:100%;
align-items: center;
justify-content: center;
display: flex;
`