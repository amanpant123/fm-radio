import React from 'react'
import styled from 'styled-components'
import {BiSearchAlt } from 'react-icons/bi'
const SearchBox = ({placeholder,handleChange}) => {
    return (
       <Container>
           <SearchContainer>
            <BiSearchAlt color="white" size="30px"/>
           <Search placeholder={placeholder} onChange={handleChange}/>
           </SearchContainer>
       </Container>
    )
}
export default SearchBox

const Container=styled.div`
min-height: 80px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
`
const Search=styled.input`
padding: 15px;
border: none;
outline: none;
background-color: #13101033;
width: 100%;
&::placeholder{
    color: white;
    font-size: 15px;
    text-align: center;
}
caret-color: white;
`
const SearchContainer=styled.div`
display: flex;
justify-content: space-around;
align-items: center;
background-color: rgba(255,255,255,0.2);
border-radius: 50px;
width: 400px;
`