import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
const GridContainerCountry = ({item}) => {
    return (
            <Grid  >
                <Link to={`/home/${item.name}`}>
                <Icon src={item.icon}/>                
                </Link>
                <Name>
                    {item.name}
                </Name>
            </Grid>
    )
}

export default GridContainerCountry


const Grid=styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`
const Icon=styled.img`
width: 100px;
height: 100px;
object-fit: cover;
border-radius: 30px;

`
const Name=styled.div`
margin-top: 10px;
`