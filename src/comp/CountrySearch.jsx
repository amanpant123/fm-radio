import React from 'react'
import styled from 'styled-components'
import GridContainerCountry from './GridContainerCountry'
const CountrySearch = ({Data}) => {
    return (<>
         <Heading>Search Country</Heading>
        <Container>
            {Data.map((item,index)=><GridContainerCountry item={item} key={index}/>)}
        </Container>
        </>
    )
}

export default CountrySearch

const Container=styled.div`
display: grid;
grid-template-columns: repeat(8,minmax(0,1fr));
width: 80%;
margin: auto;
margin-top: 50px;
`
const Heading=styled.div`
width: 79%;
font-size: 25px;
margin: auto;
`