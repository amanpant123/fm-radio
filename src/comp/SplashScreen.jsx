import React from 'react'
import styled from 'styled-components'
const SplashScreen = () => {
    return (
        <Container>
            <IconContainer>
                <Icon src="images/fm1.png"/>
                <Text>
                   MADE IN INDIA
                </Text>
            </IconContainer>
        </Container>
    )
}

export default SplashScreen
const Container=styled.div`
min-height: 100vh;
min-width: 100vw;
background-color: rgba(137, 20, 222, 0.87);
`
const IconContainer=styled.div`
display: flex;
justify-content: space-between;
flex-direction: column;
align-items: center;
min-height: 50vh;
position: relative;
top: 300px;
`
const Icon=styled.img`
max-width: 300px;
`
const Text=styled.p`
color: white;
`