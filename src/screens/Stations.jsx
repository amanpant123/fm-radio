import React from 'react'
import styled from 'styled-components'
import Header from '../comp/Header'
import SearchCountry from '../comp/SearchCountry'
const Stations = () => {
    return (
        <Container>
            <Header/>
            <SearchCountry/>
        </Container>
    )
}

export default Stations

const Container=styled.div`
min-height:100vh;
background-color:rgba(0,0,0,1);
`