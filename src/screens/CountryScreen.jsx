import React from 'react'
// import {  useParams} from 'react-router-dom'
import styled from 'styled-components'
import AllStaions from '../comp/AllStaions'
import Header from '../comp/Header'
import { MostPlayed } from '../comp/MostPlayed'
import { INDIA } from '../Data/Indiadata'
const CountryScreen = (props) => {
//    const match=useParams()
    return (
       <Container>
           <Header/>
           <Scrollable>
             <AllStaions Data={INDIA}/>
             <MostPlayed/>
           </Scrollable>
       </Container>
    )
}

export default CountryScreen

const Container=styled.div`
color: black;
max-height: 100vh;
background-color: rgba(0,0,0,1);

`
const Scrollable=styled.div`
overflow: scroll;
`
