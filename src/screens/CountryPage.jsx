import React, { useState,useEffect } from 'react'
import styled from 'styled-components'
import Header from '../comp/Header'
import CountryStation from '../Other/CountryStation'
import axios from 'axios'
import { useParams } from 'react-router'

const CountryPage = () => {
    const [most,setMost]=useState()

    const [recent,SetRecent]=useState()
    const data1=[]

  

    useEffect(()=>{
        const fetch=async()=>{
            const music =await JSON.parse(localStorage.getItem('All'));
          SetRecent(music)           
        }
        fetch()
    },[recent])
    
    const handleLocal=(item)=>{
        data1.push(item)
        localStorage.setItem('All',JSON.stringify(data1.concat(recent)))       
    }

 

    const country=useParams()
    useEffect(()=>{
        const fetchmostplayed=async()=>{
            const stationmostplayed = await axios.get('https://nl1.api.radio-browser.info/json/stations/search',{
                params:{
                    country:country.country,
                    order:'clickcount',
                    reverse:true,
                    limit:14
                }
            })
              setMost(stationmostplayed.data)
            }
        fetchmostplayed()
    },[country.country])

    return (
       <Container>
           <HeaderContainer>
           <Header screen={true}/>
           </HeaderContainer>
           <StationContainer>
               {recent?(
                   <div>
                       <h3>Recent Played</h3>
                       <div style={{display:'flex',marginTop:'40px'}}>
                           {recent?.map((item,index)=>
                           <div key={index} style={{display:'flex',flexDirection:'column',margin:"0px 90px 20px 0"}}>
                               <img src={item?.favicon} alt="" style={{width:'70px',borderRadius:'10px'}}/>
                               <p style={{marginTop:'10px'}}>{item?.name}</p>
                           </div>
                           )}
                       </div>
                   </div>
                ):''} 
              
              {/* <CountryStation data={data} text="All Stations"/>             */}
              <CountryStation data={most} text="Most Played" handleLocal={handleLocal}/>            
           </StationContainer>
       </Container>
    )
}

export default CountryPage

const Container=styled.div`
background-color: rgba(0,0,0,0.8);
min-height: 1200px;
`
const HeaderContainer=styled.div`
position: fixed;
width: 100%;
`
const StationContainer=styled.div`
padding-top:100px;
min-height: 1000px;
width: 90%;
margin: auto;
`