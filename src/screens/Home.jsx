import React,{useState,useEffect} from 'react'
import SplashScreen from '../comp/SplashScreen'
import Stations from './Stations'

const Home = () => {
    const [loading,setLoading]=useState(true)
useEffect(()=>{
   setTimeout(()=>{
     setLoading(false)
   },3000)
},[])

if(loading){
 return (
    <SplashScreen/>
 )
}
    return (
        <>
            <Stations/>
        </>
    )
}

export default Home
