import React from 'react'
import styled from 'styled-components'
import Header from '../comp/Header'
import HomeAllStations from '../Other/HomeAllStations';
import HomeMostPlayed from '../Other/HomeMostPlayed';
const HomeScreen = () => {
    const music = JSON.parse(localStorage.getItem('user'));
    
    return (
        <Container>
            <Header/>
            <DarkContainer>
                <MiddleContainer>
                   {music?
                   <div>
                       <h2>Recently Played</h2>
                       <div>
                           <Img style={{width:'70px',borderRaduis:'30px'}} src={music.favicon} alt="" />
                           <p>{music.name}</p>
                       </div>
                   </div>
                   :''}
                   <div>
                       <h2 style={{marginTop:'20px'}}>All Stations</h2>
                       <HomeAllStations country={music.country}/>
                       <h2>Most Played</h2>
                       <HomeMostPlayed country={music.country}/>
                   </div>
                </MiddleContainer>
            </DarkContainer>
        </Container>
    )
}

export default HomeScreen

const Container=styled.div`

`
const DarkContainer=styled.div`
background-color: rgba(0,0,0,0.9);
min-height: 100vh;
`
const MiddleContainer=styled.div`
width: 90%;
margin: auto;
padding-top: 20px;

`
const Img=styled.img`
border-radius:20px;
margin-top: 20px;
`