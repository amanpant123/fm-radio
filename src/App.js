import './App.css';
import Home from './screens/Home';
import {
  BrowserRouter as Router,
Routes,
  Route,
} from "react-router-dom";
import CountryPage from './screens/CountryPage';
import HomeScreen from './screens/HomeScreen';
import React from 'react'


// \
function App() {

  return (

       <Router>
       <Routes>
         <Route path="/" element={<Home/>}/>
         <Route path="/home" element={<HomeScreen/>}/>
         <Route path="/home/:country" element={<CountryPage />}/>
       </Routes>
     </Router>
  );
}

export default App;
